#!/usr/bin/env python3

"""
@author: Dasara Shullani (dasara.shullani@unifi.it)
Università degli studi di Firenze 2021
"""

import glob
import numpy as np
import os
from PIL import Image

import prnu as BestaginiPRNU


def collect_images(folder_path, img_format="JPEG"):
    main_list = []
    if isinstance(img_format, str):
        str_format_path = os.path.join(folder_path, "*." + img_format)
        main_list.extend(glob.glob(str_format_path))
    else:
        if isinstance(img_format, list):
            for item in img_format:
                str_format_path = os.path.join(folder_path, "*." + item)
                main_list.extend(glob.glob(str_format_path))
        else:
            raise ValueError("img_format value: ",
                             img_format, " is not valid.")

    return sorted(main_list)


def center_cut(data, cut_ndim):
    # np.array of RGB img (x, y, 3)
    # cut_ndim (i, j) a patch size (i x j)
    data_patch = data.copy()
    if data_patch.shape[0] > cut_ndim[0] and data_patch.shape[1] > cut_ndim[1]:
        idx_row = int(data_patch.shape[0]/2) - int(cut_ndim[0]/2) - 1
        idx_col = int(data_patch.shape[1]/2) - int(cut_ndim[1]/2) - 1
        data_patch = data_patch[idx_row:(idx_row + cut_ndim[0]),
                                idx_col:(idx_col + cut_ndim[1]):]
    else:
        raise ValueError("Cut dims too big. Original size: ", data.shape,
                         ". Cut size: ", cut_ndim)
    return data_patch


def read_images(img_pathlist, clip_flag=False, clip_size=[0, 0]):
    npdata_list = []
    for img_path in img_pathlist:
        im = Image.open(img_path)
        im_arr = np.asarray(im)
        if im_arr.dtype != np.uint8:
            raise Exception("Unable to read ", img_path)
        if im_arr.ndim != 3:
            raise Exception(img_path, " non RGB image.")
        # perform center clipping if necessary
        if not clip_flag:
            npdata_list += [im_arr]
        else:
            if im_arr.shape[0] > clip_size[0] and im_arr.shape[1] > clip_size[1]:
                im_cut = center_cut(im_arr, clip_size)
            else:
                im_cut = im_arr
            npdata_list += [im_cut]
    if len(npdata_list) == 1:
        return npdata_list[0]
    return npdata_list


def rho_distribution(ref_fingerprint):
    # eq. 6 - 7
    R = 256  # bins
    A = np.linspace(-10, 10, R + 1)
    delta_step = R / 20.0  # R / (b - a)
    rho_list = []
    for local_a in zip(A[:-1], A[1:]):
        local_b = (ref_fingerprint >= local_a[0]) & (ref_fingerprint
                                                     < local_a[1])
        local_p = np.count_nonzero(local_b)
        if local_p > 0:
            # compute revised equation 6 -> email 20-05-2021
            local_rho = (delta_step * local_p) / (ref_fingerprint.shape[0]
                                                  * ref_fingerprint.shape[1])
            rho_list.append(local_rho)
        else:
            rho_list.append(0.0)
    return rho_list


def mi_distribution(ref_list):
    # eq. 8, collects all images of a specific device
    mi_array = np.zeros((256,), dtype=float)
    for item in ref_list:
        fingerprint = BestaginiPRNU.extract_single(item)
        rho_list = rho_distribution(fingerprint)
        # print(fingerprint.min(), fingerprint.max(), np.sum(rho_list))
        mi_array = mi_array + np.asarray(rho_list)

    mi_array = mi_array / len(ref_list)
    return mi_array


def kld_distance(ref_pdfs, test_rho, clean_zeros=False):
    # eq. 12
    # init DASARA processing -->
    if clean_zeros and (np.count_nonzero(ref_pdfs) < len(ref_pdfs)
                        or np.count_nonzero(test_rho) < len(test_rho)):
        t_list = []
        r_list = []
        for i in range(len(ref_pdfs)):
            if ref_pdfs[i] != 0 and test_rho[i] != 0:
                t_list.append(test_rho[i])
                r_list.append(ref_pdfs[i])
        test_rho = np.asarray(t_list)
        ref_pdfs = np.asarray(r_list)
    # end DASARA processing.
    internal_val = test_rho * np.log(test_rho / ref_pdfs)
    kld = np.sum(internal_val)
    return kld


def get_ref_pdfs(img_list, clip_flag=True, clip_size=[500, 500]):
    arr_list = read_images(img_list, clip_flag, clip_size)
    ref_pdfs = mi_distribution(arr_list)
    return ref_pdfs


def get_kld_distance(ref_pdfs, test_img, clip_flag=True, clip_size=[500, 500],
                     clean_zeros=True):
    arr = read_images([test_img], clip_flag, clip_size)
    fingerprint = BestaginiPRNU.extract_single(arr)
    rho_list = rho_distribution(fingerprint)
    kld_val = kld_distance(ref_pdfs, rho_list, clean_zeros)
    return kld_val
