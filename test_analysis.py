#!/usr/bin/env python3

"""
@author: Dasara Shullani (dasara.shullani@unifi.it)
Università degli studi di Firenze 2021
"""
import os
import pandas as pd
import sys

if sys.argv[1] is None:
    fname = "test_kld_dresden_long.csv"
else:
    fname = str(sys.argv[1])

print("studing", fname) 
if os.path.exists(fname):
    df = pd.read_csv(fname)
    print(df)
    result = df.groupby(['Origin', 'Prediction']).count()
    print(result)
    result.to_csv("analysis_"+fname)
    pre_tag = ''
    match_val = 0
    no_match_val = 0
    for index, row in df.iterrows():
        if len(pre_tag) == 0:
            pre_tag = row['Origin']
        elif pre_tag != row['Origin']:
            print(pre_tag, match_val, no_match_val)
            # print and reset match
            match_val = 0
            no_match_val = 0
            pre_tag = row['Origin']
        else:
            if row['Origin'] == row['Prediction']:
                match_val += 1
            else:
                no_match_val += 1
