# PRNU-KLD

An implementation of Quintanar-Reséndiz, Ana L., et al. "Capture device identification from digital images using Kullback-Leibler divergence." Multimedia Tools and Applications (2021): 1-26.


```
@article{quintanar2021capture,
  title={Capture device identification from digital images using Kullback-Leibler divergence},
  author={Quintanar-Res{\'e}ndiz, Ana L and Rodr{\'\i}guez-Santos, Francisco and Pichardo-M{\'e}ndez, Josu{\'e} L and Delgado-Guti{\'e}rrez, Guillermo and Ram{\'\i}rez, Omar Jim{\'e}nez and V{\'a}zquez-Medina, Rub{\'e}n},
  journal={Multimedia Tools and Applications},
  pages={1--26},
  year={2021},
  publisher={Springer}
}
```

## Author

- Dasara Shullani (dasara.shullani@unifi.it)

## License

Copyright (C) 2021 Università degli studi di Firenze

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


## Testing with virtual-env

- Lenovo
```
cd /home/da/Documents/IAPP/virtual_envs
python3 -m venv PRNU-KLD
source PRNU-KLD/bin/activate
```
- install requirements
```
cd gitCode/PRNU-KLD
pip install -r requirements.txt
```



## Packaging Bestagini-PRNU

Main source --> https://setuptools.readthedocs.io/en/latest/userguide/quickstart.html

- install setuptools in (PRNU-KLD) enviroment
```
pip install --upgrade setuptools
```

- create *pyproject.toml* in the "Bestagini-PRNU" folder
```
[build-system]
requires = ["setuptools", "wheel"]
build-backend = "setuptools.build_meta"
```

- create *setup.cfg* in the "Bestagini-PRNU" folder. **Note**: in `packages = prnu`, *prnu* is the subfolder containing the functions we want to export.
```
[metadata]
name = BestaginiPRNU
version = 1.1.0

[options]
packages = prnu
install_requires =
    joblib
    Pillow
    PyWavelets
    scikit-learn
    scipy
    threadpoolctl
    tqdm
    numpy; python_version == "3.8"
```

- install build in (PRNU-KLD) enviroment
```
pip install build
```

- build BestaginiPRNU
```
cd ~/Documents/Research/IAPP/gitRepo/Bestagini-PRNU
python -m build
```

- whl file ready in subfolder `dist`
```
(PRNU-KLD):~/Documents/Research/IAPP/gitRepo/Bestagini-PRNU$ ls dist/
BestaginiPRNU-1.1.0-py3-none-any.whl
BestaginiPRNU-1.1.0.tar.gz

```


## Use Package

- obtain whl and install it in (PRNU-KLD) enviroment
```
pip install dist/BestaginiPRNU-1.1.0-py3-none-any.whl
```

- import in your py-file as
```
import prnu as BestaginiPRNU
```


## Profiling

- run profiling
```
python -m cProfile -o example_profile example.py
```

- study profiling
```
import pstats
from pstats import SortKey
p = pstats.Stats('example_profile')
p.sort_stats(SortKey.TIME).print_stats(10)
```
