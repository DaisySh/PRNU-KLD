#!/usr/bin/env python3

"""
@author: Dasara Shullani (dasara.shullani@unifi.it)
Università degli studi di Firenze 2021
"""
import os
import numpy as np
import kld


def build_dresden_dataset(img_path_list):
    dataset_dict = {}
    for item in img_path_list:
        img_name = os.path.basename(item).split("_")
        key = "_".join(img_name[:-1])
        if key not in dataset_dict.keys():
            dataset_dict[key] = [item]
        else:
            dataset_dict[key].append(item)
    return dataset_dict


dresden_path = "/images/images/forensic_datasets/Dresden_image_database/"
dresden_data_list = kld.functions.collect_images(dresden_path, "JPG")
dresden_data_list += kld.functions.collect_images(dresden_path, "jpg")

dresden = build_dresden_dataset(dresden_data_list)
test_dresden = {}
num_ref = 30
dresden_pdfs = {}

for key in dresden.keys():
    np.random.shuffle(dresden[key])
    if len(dresden[key]) >= num_ref:
        data_list = dresden[key][:num_ref]
        # test_dresden[key] = dresden[key][num_ref:]
        test_dresden[key] = dresden[key][num_ref:num_ref+10]
    else:
        tmp_len = len(dresden[key]) - 5
        data_list = dresden[key][:tmp_len]
        test_dresden[key] = dresden[key][tmp_len:]
    print(key)
    dresden_pdfs[key] = kld.functions.get_ref_pdfs(data_list)


with open("test_kld_dresden.csv", "a+") as f:
    for j_key in test_dresden.keys():
        for item in test_dresden[j_key]:
            local_kld = 1000
            device_key = ''
            for key in dresden_pdfs.keys():
                test_kld = kld.functions.get_kld_distance(dresden_pdfs[key], item)
                if test_kld < local_kld:
                    local_kld = test_kld
                    device_key = key
            print(os.path.basename(item), j_key, local_kld, device_key)
            s = "{},{},{},{}\n".format(os.path.basename(item), j_key, local_kld, device_key)
            f.write(s)
